package com.flan.seckill.dao;

import com.flan.seckill.entity.Seckill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 配置Spring与Junit的整合
 * Created by flan on 2016/12/29.
 */
//1.告诉Junit Spring的配置文件存放位置
@ContextConfiguration({"classpath:spring/spring-dao.xml"})
//2.Junit启动时，让SpringIoC容器使用指定的配置文件启动
@RunWith(SpringJUnit4ClassRunner.class)
public class SeckillDaoTest {

    //3.将SpringIoC容器中的DAO对象注入到本测试类中
    @Resource
    private SeckillDao seckillDao;

    @Test
    public void reduceNumber() throws Exception {
        int count = seckillDao.reduceNumber(1000,new Date());
        System.out.println("执行减库存操作 "+count);
    }

    @Test
    public void queryById() throws Exception {

        Seckill seckill = seckillDao.queryById(1000);
        System.out.println(seckill.getName());
        System.out.println(seckill);
    }

    @Test
    public void queryAll() throws Exception {

        List<Seckill> seckillList = seckillDao.queryAll(0,10);
        for(Seckill s : seckillList){
            System.out.println(s.getName());
        }

    }

}