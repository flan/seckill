package com.flan.seckill.dao;

import com.flan.seckill.entity.SuccessKilled;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by flan on 2016/12/29.
 */
@ContextConfiguration({"classpath:spring/spring-dao.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class SuccesKilledDaoTest {

    @Resource
    private SuccesKilledDao succesKilledDao;

    @Test
    public void insertSuccessKilled() throws Exception {

        int count = succesKilledDao.insertSuccessKilled(1000,13667167773L);
        System.out.println("执行秒杀操作："+count);

    }

    @Test
    public void queryByIdWithSeckill() throws Exception {
        SuccessKilled successKilled = succesKilledDao.queryByIdWithSeckill(1000,13667167773L);
        System.out.println("成功秒杀的用户： "+successKilled.getUserPhone());
        System.out.println("商品名： "+successKilled.getSeckill().getName());
    }

}