package com.flan.seckill.service;

import com.flan.seckill.dto.Exposer;
import com.flan.seckill.dto.SeckillExecution;
import com.flan.seckill.entity.Seckill;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.List;

/**
 * 测试秒杀服务接口实习类
 * Created by flan on 2017/1/4.
 */
@ContextConfiguration({"classpath:spring/spring-dao.xml",
                        "classpath:spring/spring-service.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class SeckillServiceTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private SeckillService seckillService;

    @Test
    public void getAllSeckillList() throws Exception {

        List<Seckill> list = seckillService.getAllSeckillList();
        logger.info("list{}",list);

    }

    @Test
    public void getSeckillById() throws Exception {
        Seckill seckill = seckillService.getSeckillById(1000);
        logger.info("seckill",seckill);
    }

    @Test
    public void exportSeckillUrl() throws Exception {
        Exposer exposer = seckillService.exportSeckillUrl(1000);
        logger.info("exposer={}",exposer);
    }

    @Test
    public void executeSeckill() throws Exception {
        SeckillExecution seckillExecution = seckillService.executeSeckill(1000,13667167774L,"d7c99f346c2da098ef25a3fe178c3b88");
        logger.info("seckillExecution={}",seckillExecution);
    }

    @Test
    public void seckillLogic() throws Exception {
        Exposer exposer = seckillService.exportSeckillUrl(1000);
        logger.info("exposer={}",exposer);

        if(exposer.isExposed()){
            SeckillExecution seckillExecution = seckillService.executeSeckill(1000,13667167774L,exposer.getMd5());
        }

    }

}