//秒杀相关的js逻辑代码
// js 模块化实现方式：通过逐层定义对象的方式
var seckill = {     //定义一个seckill对象

    //封装秒杀相关的ajax的url，方便统一管理级修改
    URL : {         //在seckill对象中在定义一个URL对象
        now : function () { //封装获取系时间的url
            return "/seckill/time/now";
        },
        exposer : function (seckillId) {
            return "/seckill/"+seckillId+"/exposer";
        },
        execution : function (seckillId,md5) {
            return "/seckill/"+seckillId+"/"+md5+"/execute";
        }
    },
    validatePhone : function(phone){   //创建一个用来验证手机号是否正确的工具类，单独出来可以被共用
        if(phone && phone.length == 11 && !isNaN(phone)){
            return true;
        }else {
            return false;
        }
    },
    handleSeckill : function (seckillId,node) {   //封装执行秒杀操作的逻辑,node参数为DOM节点

        //获取秒杀地址，显示执行结果
        node.hide().html("<button class=\"btn btn-primary but-lg\" id=\"killBtn\">立即秒杀</button>");

        $.post(seckill.URL.exposer(seckillId),{},function (result) {
            //获取秒杀地址
            if(result && result.success){

                var exposer = result.data;
                var md5 = exposer.md5;

                if(exposer.exposed){
                    //开始秒杀
                    var killUrl = seckill.URL.execution(seckillId,md5);
                    //one可以实现绑定一次操作，防止重复提交，click方式则是长久绑定
                    $("#killBtn").one("click",function () {
                        //1.显示按钮已点击效果
                        $(this).addClass("disabled");
                        //2.发生秒杀请求
                        $.post(killUrl,{},function (result) {
                            if(result && result.success){
                                var killResult = result.data;
                                var state = killResult.state;
                                var stateInfo = killResult.stateInfo;
                                //3.显示秒杀结果
                                node.html("<span class='label label-success'>"+stateInfo+"</span>")
                            }
                        })

                    });

                    node.show();    //显示节点

                }else {
                    //未开始秒杀，由于不同PC上的计算误差,如客户机计时偏快
                    var nowTime = exposer.now;
                    var startTime = exposer.start;
                    var endTime = exposer.end;
                    //更新客户端的计时器
                    seckill.countdown(seckillId,nowTime,startTime,endTime);
                }
            }else {
                console.log("result : "+result);
            }
        });

    },
    countdown : function (seckillId,nowTime,startTime,endTime) {       //封装倒计时逻辑

        var seckillBox = $("#seckill-box");
        if(nowTime > endTime){
            //秒杀活动结束
            seckillBox.html("秒杀活动结束!");
        }else if(nowTime < startTime){

            //秒杀活动还未开始，执行计算操作
            var killTime = new Date(startTime + 1000);  //延迟一秒，防止网络延迟等异常情况

            //调用jquery插件的函数
            seckillBox.countdown(killTime,function (event) {
                //时间格式化
                var format = event.strftime("距离秒杀开始时间还有 ：%D天 %H时 %M分 %S秒");
                seckillBox.html(format);
            }).on("finish.countdown",function () {
                //倒计时结束时执行的回调操作
                seckill.handleSeckill(seckillId,seckillBox);
            });
        }else {
            //正在秒杀时间段内
            seckill.handleSeckill(seckillId,seckillBox);
        }

    },
    //详情页的秒杀逻辑
    detail : {      //在seckill对象中再定义一个detail对象

        //详情页初始化操作
        init : function(params){

            //手机验证和登录
            var killPhone = $.cookie("killPhone");//cookie中查找

            if(!seckill.validatePhone(killPhone)){
                //未登录，显示登录框
                var killPhoneModal = $("#killPhoneModal");
                killPhoneModal.modal({
                    show : true,    //显示弹出层
                    backdrop : "static",    //禁止点击其他位置关闭窗口
                    keyboard : false    //关闭键盘事件
                });

                //绑定提交按钮
                $("#killPhoneBtn").click(function(){
                    var inputPhone = $("#killPhoneKey").val();
                    if(seckill.validatePhone(inputPhone)){

                        //将电话号码写入cookie，即登录操作
                        //有效期7天，绑定域为/seckill
                        $.cookie("killPhone",inputPhone,{expires:7,path:"/seckill"});
                        //刷新页面
                        window.location.reload();
                    } else {
                        $("#killPhoneMessage").hide().html("<label class=\"label label-danger\">请填写正确的手机号</label>").show(300);
                    }
                });
            }else {
                //用户已登录，执行倒计时操作
                $.get(seckill.URL.now(),{},function (result) {
                    if(result && result.success){

                        var nowTime = result.data;
                        //params是init函数的参数
                        var startTime = params.startTime;
                        var endTime = params.endTime;
                        var seckillId = params.seckillId;

                        //执行计算交互操作
                        seckill.countdown(seckillId,nowTime,startTime,endTime);
                    }else {
                        console.log("result: "+result);
                    }
                });
            }
        }
    }
}
