package com.flan.seckill.web;

import com.flan.seckill.dto.Exposer;
import com.flan.seckill.dto.SeckillExecution;
import com.flan.seckill.dto.SeckillResult;
import com.flan.seckill.entity.Seckill;
import com.flan.seckill.enums.SeckillStateEnum;
import com.flan.seckill.exception.RepeatKillException;
import com.flan.seckill.exception.SeckillCloseException;
import com.flan.seckill.service.SeckillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 秒杀功能的控制层实现
 * Created by flan on 2017/1/4.
 */
@Controller
@RequestMapping("/seckill")
public class SeckillController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private SeckillService seckillService;

    /**
     * 显示列表页
     */
    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String list(Model model){

        List<Seckill> seckillList = seckillService.getAllSeckillList();
        model.addAttribute("list",seckillList);

        return "list";
    }

    @RequestMapping(value = "/{seckillId}/detail",method = RequestMethod.GET)
    public String detail(@PathVariable("seckillId") Long seckillId, Model model){

        if(seckillId == null){
            return "redirect:/seckill/list";
        }

        Seckill seckill = seckillService.getSeckillById(seckillId);
        if(seckill == null){
            return "forward:/seckill/list";
        }

        model.addAttribute("seckill",seckill);
        return "detail";
    }

    @RequestMapping(value = "/{seckillId}/exposer",
                    method = RequestMethod.POST,
                    produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public SeckillResult<Exposer> exposer(@PathVariable("seckillId")Long seckillId){

        SeckillResult<Exposer> result;

        try{
            Exposer exposer = seckillService.exportSeckillUrl(seckillId);
            result = new SeckillResult<Exposer>(true,exposer);
        }catch (Exception e){
            Exposer exposer = seckillService.exportSeckillUrl(seckillId);
            result = new SeckillResult<Exposer>(false,e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/{seckillId}/{md5}/execute",method = RequestMethod.POST,
                    produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public SeckillResult<SeckillExecution> execute(@PathVariable("seckillId") Long seckillId,
                                                   @PathVariable("md5") String md5,
                                                   @CookieValue(value = "killPhone",required = false) Long phone){

        SeckillResult<SeckillExecution>  reseule;

        if(phone == null){
            return new SeckillResult<SeckillExecution>(false,"用户未登陆");
        }

        try{

            SeckillExecution execution = seckillService.executeSeckill(seckillId,phone,md5);
            return new SeckillResult<SeckillExecution>(true,execution);

        } catch (RepeatKillException e){

            SeckillExecution execution = new SeckillExecution(seckillId, SeckillStateEnum.REPEAT_KILL);
            return new SeckillResult<SeckillExecution>(true,execution);

        } catch (SeckillCloseException e){

            SeckillExecution execution = new SeckillExecution(seckillId, SeckillStateEnum.END);
            return new SeckillResult<SeckillExecution>(true,execution);

        } catch (Exception e){
            logger.error(e.getMessage(),e);
            return new SeckillResult<SeckillExecution>(true,e.getMessage());
        }

    }

    @RequestMapping(value = "/time/now",method = RequestMethod.GET)
    @ResponseBody
    public SeckillResult<Long> time(){
        Date date = new Date();
        return new SeckillResult(true,date.getTime());
    }

}
