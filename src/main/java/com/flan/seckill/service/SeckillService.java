package com.flan.seckill.service;

import com.flan.seckill.dto.Exposer;
import com.flan.seckill.dto.SeckillExecution;
import com.flan.seckill.entity.Seckill;
import com.flan.seckill.exception.SeckillCloseException;
import com.flan.seckill.exception.SeckillException;

import java.util.List;

/**
 * 秒杀系统的服务层接口
 * Created by flan on 2017/1/3.
 */
public interface SeckillService {

    /**
     * 查询所有的秒杀产品
     * @return
     */
    List<Seckill> getAllSeckillList();

    /**
     * 查询单个的秒杀商品
     * @param seckillId  秒杀商品的Id
     * @return
     */
    Seckill getSeckillById(long seckillId);

    /**
     * 当秒杀开启时，返回秒杀的URL
     * 当秒杀未开启时，返回秒杀的开始或结束时间
     * @param seckillId
     * @return 封装了返回参数的DTO对象
     */
    Exposer exportSeckillUrl(long seckillId);

    /**
     * 执行秒杀操作
     * @param seckillId
     * @param userPhone
     * @param md5
     * @return  秒杀结果的 DTO 对象
     */
    SeckillExecution executeSeckill(long seckillId, long userPhone, String md5)
            throws SeckillException,RuntimeException,SeckillCloseException;

}
