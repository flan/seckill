package com.flan.seckill.exception;

/**
 * 秒杀业务中的异常
 * Created by flan on 2017/1/3.
 */
public class SeckillException extends RuntimeException {

    public SeckillException(String message) {
        super(message);
    }

    public SeckillException(String message, Throwable cause) {
        super(message, cause);
    }
}
