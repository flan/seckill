package com.flan.seckill.exception;

/**
 * 重复秒杀异常
 * Created by flan on 2017/1/3.
 */
public class RepeatKillException extends SeckillException {

    public RepeatKillException(String message) {
        super(message);
    }

    public RepeatKillException(String message, Throwable cause) {
        super(message, cause);
    }
}
