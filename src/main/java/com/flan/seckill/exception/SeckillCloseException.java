package com.flan.seckill.exception;

/**
 * 秒杀关闭异常
 * Created by flan on 2017/1/3.
 */
public class SeckillCloseException extends SeckillException {

    public SeckillCloseException(String message) {
        super(message);
    }

    public SeckillCloseException(String message, Throwable cause) {
        super(message, cause);
    }
}
