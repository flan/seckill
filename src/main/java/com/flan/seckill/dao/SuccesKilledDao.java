package com.flan.seckill.dao;

import com.flan.seckill.entity.SuccessKilled;
import org.apache.ibatis.annotations.Param;

/**
 * Created by flan on 2016/12/28.
 */
public interface SuccesKilledDao {

    /**
     * 插入秒杀信息
     * @param seckillId
     * @param userPhone
     * @return
     */
    int insertSuccessKilled(@Param("seckillId") long seckillId,@Param("userPhone") long userPhone);

    /**
     * 根据id查询秒杀成功信息
     * @param seckillId
     * @return
     */
    SuccessKilled queryByIdWithSeckill(@Param("seckillId")long seckillId,@Param("userPhone")long userPhone);

}
